
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class UiTest {
    //parent test class to get setup and quit
    protected WebDriver driver;
    MainPage mainPage;

    @BeforeEach
    void setup() {
        driver = new ChromeDriver();
        String url = "https://orlovrs.github.io/time-tracker/";
        driver.get(url);
        mainPage = new MainPage(driver);
    }

    @AfterEach
    void teardown() {
        driver.quit();
    }
}